// .eslintrc.js example
module.exports = {
  "env": {
      "browser": true,
      "es2021": true
  },
  "extends": "eslint:recommended",
  "extends": "airbnb",
  "plugins": ["react"],
  "rules": { "react/jsx-filename-extension": [0] },
  "parserOptions": {
    "ecmaFeatures": {
      "jsx": true
    }
  }
}

